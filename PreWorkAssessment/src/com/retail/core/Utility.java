package com.retail.core;

import com.retail.core.Item.shippingMethod;
/**
 * 
 * @author Sukanth Gunda
 * @version 1.0
 * File Name : Utility.java
 */
public class Utility {
	private static double shippingCost;
	private static String pCodeToString = null;
	
	/**
	 * Description : Method to calculate shipping cost
	 * @param shipMethod
	 * @return shippingCost
	 */
	public static double calShippingCost(shippingMethod shipMethod, long pCode,float pWeight) {
		long secondToLastDigtInProductCode;
		pCodeToString = new Long(pCode).toString();
		pCodeToString = pCodeToString.substring(pCodeToString.length() - 2, pCodeToString.length() - 1);

		secondToLastDigtInProductCode = Long.parseLong(pCodeToString);
		shippingCost = (shipMethod.toString().equalsIgnoreCase("AIR")) ? (pWeight * secondToLastDigtInProductCode): (pWeight * 2.5);
		shippingCost = Double.parseDouble(String.format("%.2f", shippingCost));
		return shippingCost;
	}
}
