package com.retail.core;

import java.util.Comparator;
/**
 * 
 * @author Sukanth Gunda
 * @version 1.0
 * File Name : ProductCodeComparator.java
 *
 */
public class ProductCodeComparator implements Comparator<Item>{
	/**
	 * Overriding the compare method of the comparator class to sort based on the UPC
	 */
	@Override
	public int compare(Item o1, Item o2) {
		Item item1 = (Item)o1;
		Item item2 = (Item)o2;
		if(item1.getpCode() == item2.getpCode()) {
			return 0;
		}
		else if(item1.getpCode() > item2.getpCode()){
			return 1;
		}
		else {
			return -1;
		}
	}
}
